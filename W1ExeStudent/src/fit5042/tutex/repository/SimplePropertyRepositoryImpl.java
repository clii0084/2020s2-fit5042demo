/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.repository;

import fit5042.tutex.repository.entities.Property;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO Exercise 1.3 Step 2 Complete this class.
 * 
 * This class implements the PropertyRepository class. You will need to add the keyword
 * "implements" PropertyRepository. 
 * 
 * @author Chengguang Li
 */
public class SimplePropertyRepositoryImpl implements PropertyRepository {
    
	ArrayList<Property> properties = new ArrayList<Property>();
	
    public SimplePropertyRepositoryImpl() {
        
    }
    
    // override addProperty Method 
    @Override
    public void addProperty(Property property) throws Exception{
           	
    	properties.add(property);
    }
    
    // override searchPropertyById Method 
    @Override
    public Property searchPropertyById(int id) throws Exception{
       boolean findId = false; 
       Property pt = new Property(0,"",0,0,0.0);
       
       for(int i=0; i< properties.size();i++) {
    	   if( properties.get(i).getId() == id ) {
    		     findId = true; 
    		     pt =  properties.get(i);
    	   }   	    	   
       } 
    	
       // property found by ID exists or not 
       if (findId == true) {
    	  return pt; 
       }else {
    	   return null;
       }
    	
    }
    // override getAllProperties Method 
    @Override
    public List<Property> getAllProperties() throws Exception{
    	
        return properties;   	
    }
    
}
