package fit5042.tutex;

import fit5042.tutex.repository.PropertyRepository;
import fit5042.tutex.repository.PropertyRepositoryFactory;
import fit5042.tutex.repository.entities.Property;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.channels.NonWritableChannelException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * TODO Exercise 1.3 Step 3 Complete this class. Please refer to tutorial instructions.
 * This is the main driver class. This class contains the main method for Exercise 1A
 * 
 * This program can run without the completion of Exercise 1B.
 * 
 * @author Chengguang Li
 */
public class RealEstateAgency {
    private String name;
    private final PropertyRepository propertyRepository;

    public RealEstateAgency(String name) throws Exception {
        this.name = name;
        this.propertyRepository = PropertyRepositoryFactory.getInstance();
    }
    
    // this method is for initializing the property objects
    // complete this method
    public void createProperty() {
    	try {
    	propertyRepository.addProperty(new Property(1, "24 Boston Ave, Malvern East VIC 3415, Australia", 2, 150,420000.00));
    	propertyRepository.addProperty(new Property(2, "11 Bettina St, Clayton VIC 3168, Australia",3, 352, 360000.00));
    	propertyRepository.addProperty(new Property(3, "Wattle Ave, Glen Huntly VIC 3163, Australia", 5, 800, 650000.00));
    	propertyRepository.addProperty(new Property(4, "Hamilton St, Bentleigh VIC 3204,Australia", 2, 170, 435000.00));
    	propertyRepository.addProperty(new Property(5, "Spring Rd, Hampton East VIC 3188, Australia", 1, 60, 820000.00));
    	System.out.println("Worked by Chengguang Li. Student ID: 30488869 ");
    	System.out.println("5 Properties added successfully!");
    }catch (Exception e) {
		// TODO: handle exception
    	System.out.println(e);
	}
    	
    }
    
    // this method is for displaying all the properties
    // complete this method
    public void displayProperties() {   	
       List<Property> pt = new ArrayList<Property>();
       try {
       pt = propertyRepository.getAllProperties();
       for(int i = 0; i< pt.size(); i++) {
    	  System.out.println( pt.get(i).toString());      
       }
       }catch (Exception e) {
		// TODO: handle exception
    	  System.out.println(e);
	}
    }
    
    // this method is for searching the property by ID
    // complete this method
    public void searchPropertyById() {
        try {
        	 System.out.println("Please enter the ID of property");
             String s = new Scanner(System.in).next();
             int idd = Integer.parseInt(s);
             Property pt = propertyRepository.searchPropertyById(idd);
             System.out.println(pt.toString());
        }catch (Exception e) {
			// TODO: handle exception
        	System.out.println("Sorry, your input cannot find");
        }
    	
        
    }
    
    public void run() {
        createProperty();
        System.out.println("********************************************************************************");
        displayProperties();
        System.out.println("********************************************************************************");
        searchPropertyById();
    }
    
    public static void main(String[] args) {
        try {
            new RealEstateAgency("FIT5042 Real Estate Agency").run();
        } catch (Exception ex) {
            System.out.println("Application fail to run: " + ex.getMessage());
        }
    }
}
