package fit5042.tutex.calculator;
import javax.ejb.Stateful;
import java.util.ArrayList;
import fit5042.tutex.repository.entities.Property;


@Stateful
public class ComparePropertySessionBean implements CompareProperty{
	ArrayList<Property> properties = new ArrayList<Property>();
	public ComparePropertySessionBean() {
		
	}
	
	@Override
    public void addProperty(Property property) {
    	
    	try {
    		properties.add(property);
    		
    	}catch (Exception e) {
    		
    	}
    }
	
	@Override
	public void removeProperty(Property property) {
		try {
    		properties.remove(property);
    		
    	}catch (Exception e) {
    		
    	}
		
	}
	
	// find the cheapest unit price of the room by comparing different properties 
	@Override
	public int bestPerRoom() {
	  int index = 0;
	  int result=0; 
	  try {
	     Double temp = properties.get(0).getPrice()/properties.get(0).getNumberOfBedrooms();  
	  for(int i=1;i< properties.size();i++) {
		 Double tempTest = properties.get(i).getPrice()/properties.get(i).getNumberOfBedrooms(); 
		 if(tempTest < temp) {
			 temp = tempTest; 
			 index = i;
		 }
	  }
	     
	  }catch (Exception e){
		  
	  }
     result = properties.get(index).getPropertyId();
	  
	  return result;	
	}
		
} 
