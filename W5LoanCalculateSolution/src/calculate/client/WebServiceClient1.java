package calculate.client;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;

public class WebServiceClient1 {
	private WebTarget webTarget;
    private Client client;
    private static final String BASE_URI = "http://0.0.0.0:8080/W5LoanCalculateSolution/webresources";
    
    public WebServiceClient1() {
        client = javax.ws.rs.client.ClientBuilder.newClient();
        webTarget = client.target(BASE_URI).path("calculate");
    }
    public String getHtml() throws ClientErrorException {
        WebTarget resource = webTarget;
        return resource.request(javax.ws.rs.core.MediaType.TEXT_HTML).get(String.class);
    }
    
    public void setPostValue() throws ClientErrorException {
        webTarget.request(javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED).post(null);
    }
       
    public void setPostValue2(String principle,String interestRate,String numberOfYears) throws ClientErrorException {
        Form form = new Form();
        form.param("principle", principle);
        form.param("interestRate", interestRate);
        form.param("numberOfYears", numberOfYears);
        webTarget.request(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE));
    }
    
    public void putHtml(Object requestEntity) throws ClientErrorException {
        webTarget.request(javax.ws.rs.core.MediaType.TEXT_HTML).put(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.TEXT_HTML));
    }
    
    public void close() {
        client.close();
    }
    
}
