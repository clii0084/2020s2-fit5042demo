package calculate;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import calculate.client.WebServiceClient1;


@Named(value = "webCalculateServiceBean")
@SessionScoped
public class WebCalculateServiceBean implements Serializable{

	private double principle;
    private double interestRate;
    private int numberOfYears;
    private WebServiceClient1 webServiceClient1;
    
   public WebCalculateServiceBean() {
	   
   }
   
   public double getPrinciple() {
       return principle;
   }

   public void setPrinciple(double principle) {
       this.principle = principle;
   }

   public double getInterestRate() {
       return interestRate;
   }

   public void setInterestRate(double interestRate) {
       this.interestRate = interestRate;
   }

   public int getNumberOfYears() {
       return numberOfYears;
   }

   public void setNumberOfYears(int numberOfYears) {
       this.numberOfYears = numberOfYears;
   }
   
   
   public void setWebServiceClient1() {
	    webServiceClient1 = new WebServiceClient1();
	    webServiceClient1.setPostValue2(String.valueOf(getPrinciple()),String.valueOf(getInterestRate()),String.valueOf(getNumberOfYears()));
	   
   }
    
}
