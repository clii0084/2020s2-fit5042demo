package calculate;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;

/**
 * Session Bean implementation class LoanStorageBean
 */
@Singleton
@LocalBean
public class LoanStorageBean {
    
	private double principle;
    private double interestRate;
    private int numberOfYears;
    private double monthlyPayment;
    
    public double getPrinciple() {
        return principle;
    }

    public void setPrinciple(double principle) {
        this.principle = principle;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }

    public int getNumberOfYears() {
        return numberOfYears;
    }

    public void setNumberOfYears(int numberOfYears) {
        this.numberOfYears = numberOfYears;
    }

    public double getMonthlyPayment() {
    	setMonthlyPayment();
        return monthlyPayment;
    }

    public void setMonthlyPayment() {
    	double monthlyPayment = 0.0;
        int numberOfPayments = numberOfYears * 12;
        if (interestRate > 0) {
            monthlyPayment = principle * (interestRate * (Math.pow((1 + interestRate), numberOfPayments))) / ((Math.pow((1 + interestRate), numberOfPayments)) - 1);
        } else {
            monthlyPayment = 1000;
        }
        this.monthlyPayment = monthlyPayment;
    }

}
