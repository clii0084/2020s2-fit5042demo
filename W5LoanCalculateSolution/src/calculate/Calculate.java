package calculate;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

@Path("calculate")
public class Calculate {
    @SuppressWarnings("unused")
    @Context
    private UriInfo context;
    @EJB
    private LoanStorageBean loanStorageBean;
    /**
     * Default constructor. 
     */
    public Calculate() {
        // TODO Auto-generated constructor stub
    }

    /**
     * Retrieves representation of an instance of Calculate
     * @return an instance of String
     */
    @GET
    @Produces("text/html")
    public String getHtml() {
        // TODO return proper representation object
    	return "<html><body><h1>Moneth Payment = " + loanStorageBean.getMonthlyPayment() + "!</h1></body></html>";
    }

    /**
     * PUT method for updating or creating an instance of Calculate
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("text/html")
    public void putHtml(String content) {
    }
    @POST
    @Consumes("application/x-www-form-urlencoded")
    public void setPostValue( @FormParam("principle") String principle,@FormParam("interestRate") String interestRate,@FormParam("numberOfYears") String numberOfYears) {
     loanStorageBean.setPrinciple(Double.valueOf(principle));
     loanStorageBean.setInterestRate(Double.valueOf(interestRate));
     loanStorageBean.setNumberOfYears(Integer.parseInt(numberOfYears));
    }

}